/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import javax.swing.JOptionPane;
/**
 *
 * @author vitic
 */
public class TestDbProducto {
    public static void main(String[] args) {
        // TODO code application logic here
        dbProducto db = new dbProducto();
        Productos pro = new Productos();
        
        // Test siExiste
        try{
            if(db.siExiste(1)){
                JOptionPane.showMessageDialog(null, "Si existe");
            }
            else{
                JOptionPane.showMessageDialog(null, "No existe");
            }
        }catch(Exception e){
            System.out.println("Surgio un error " + e.getMessage());
        }
        
        
        // Buscar
        try{
            pro = (Productos) db.buscar("201");
            if(pro.getIdProductos() == 0){
                JOptionPane.showMessageDialog(null, "El producto buscado no existe");
            }
            else{
                JOptionPane.showMessageDialog(null, "Producto " + pro.getNombre() + " Precio " + pro.getPrecio());
            }
        }catch(Exception e){
            System.out.println("Surgio un error " + e.getMessage());
        }
        
        
        /*
        //Insertar
        if(db.conectar()){
            System.out.println("Fue posible conectarse");
            
            pro.setCodigo("1000");
            pro.setNombre("Atun");
            pro.setPrecio(10.50f);
            pro.setStatus(0);
            
            try{
                db.insertar(pro);
                JOptionPane.showMessageDialog(null, "Se agrego con exito");
            } catch(Exception e){
                JOptionPane.showMessageDialog(null, "Surgio un error " + e.getMessage());
            }
        }
        
        // Actualizar - Cambia el ID = 1
        pro.setIdProductos(1);
        pro.setNombre("Jabon Ariel 1kg");
        pro.setCodigo("201");
        pro.setFecha("2024-06-25");
        pro.setPrecio(45.40f);
        
        try{
            db.actualizar(pro);
            JOptionPane.showMessageDialog(null, "Se actualizo con exito");
        } catch(Exception e){
            JOptionPane.showMessageDialog(null, "Surgio un error " + e.getMessage());
        }
        
        // Habilitar y Deshbilitar
        // - Habilitar
        pro.setIdProductos(3);
        
        try{
            db.habilitar(pro);
            JOptionPane.showMessageDialog(null, "Se habilito con exito");
        } catch(Exception e){
            JOptionPane.showMessageDialog(null, "Surgio un error " + e.getMessage());
        }
        
        // - Deshabilitar 
        pro.setIdProductos(2);
        
        try{
            db.deshabilitar(pro);
            JOptionPane.showMessageDialog(null, "Se deshabilito con exito");
        } catch(Exception e){
            JOptionPane.showMessageDialog(null, "Surgio un error " + e.getMessage());
        }
        
        // Borrar registros
        // db.borrarRegistros();
        */
    }
}
