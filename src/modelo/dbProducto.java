/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.ArrayList;

/**
 *
 * @author vitic
 */
public class dbProducto extends dbManejador implements Persistencia{
    public dbProducto(){
        super();
    }
    
    @Override
    public void insertar(Object object) throws Exception{
        Productos pro = new Productos();
        pro = (Productos)object;
        
        String consulta = "";
        consulta = "Insert into productos(codigo,nombre,fecha,precio,status)" + " values(?,?,?,?,?)";
        
        if(this.conectar()){
            this.sqlCosulta = this.conexion.prepareStatement(consulta);
            this.sqlCosulta.setString(1, pro.getCodigo());
            this.sqlCosulta.setString(2, pro.getNombre());
            this.sqlCosulta.setString(3, pro.getFecha());
            this.sqlCosulta.setFloat(4, pro.getPrecio());
            this.sqlCosulta.setInt(5, pro.getStatus());
            
            this.sqlCosulta.executeUpdate();
            this.desconectar();
        }
    }

    @Override
    public void actualizar(Object object) throws Exception {
        Productos pro = new Productos();
        pro = (Productos)object;
        
        String consulta = "";
        consulta = "Update productos set codigo = ?, nombre = ?, fecha = ?, precio = ? where idProductos = ? and status = 0";
        
        if(this.conectar()){
            this.sqlCosulta = this.conexion.prepareStatement(consulta);
            this.sqlCosulta.setString(1, pro.getCodigo());
            this.sqlCosulta.setString(2, pro.getNombre());
            this.sqlCosulta.setString(3, pro.getFecha());
            this.sqlCosulta.setFloat(4, pro.getPrecio());
            this.sqlCosulta.setInt(5, pro.getIdProductos());
            
            this.sqlCosulta.executeUpdate();
            this.desconectar();
        }
    }

    @Override
    public void habilitar(Object object) throws Exception {
        Productos pro = new Productos();
        pro = (Productos)object;
        
        String consulta = "";
        consulta = "Update productos set status = 0 where idProductos = ? and status = 1";
        
        if(this.conectar()){
            this.sqlCosulta = this.conexion.prepareStatement(consulta);
            this.sqlCosulta.setInt(1, pro.getIdProductos());
            
            this.sqlCosulta.executeUpdate();
            this.desconectar();
        }
    }

    @Override
    public void deshabilitar(Object object) throws Exception {
        Productos pro = new Productos();
        pro = (Productos)object;
        
        String consulta = "";
        consulta = "Update productos set status = 1 where idProductos = ? and status = 0";
        
        if(this.conectar()){
            this.sqlCosulta = this.conexion.prepareStatement(consulta);
            this.sqlCosulta.setInt(1, pro.getIdProductos());
            
            this.sqlCosulta.executeUpdate();
            this.desconectar();
        }
    }
    
    @Override
    public void borrarRegistros(Object object) throws Exception {
        Productos pro = new Productos();
        pro = (Productos)object;
        
        String consulta = "";
        consulta = "delete productos";
        
        if(this.conectar()){
            this.sqlCosulta = this.conexion.prepareStatement(consulta);
            
            this.sqlCosulta.executeUpdate();
            this.desconectar();
        }
    }
    
    @Override
    public boolean siExiste(int id) throws Exception {
        boolean exito = false;
        
        String consulta = "";
        consulta = "select * from productos where idProductos = ? and status = 0";
        
        if(this.conectar()){
            this.sqlCosulta = this.conexion.prepareStatement(consulta);
            this.sqlCosulta.setInt(1, id);
            
            registros = this.sqlCosulta.executeQuery();
            if(registros.next()) exito = true;
            
            this.desconectar();
        }
        
        return exito;
    }

    @Override
    public ArrayList lista() throws Exception {
        ArrayList listaProductos = new ArrayList<Productos>();
        String consulta = "Select * from productos where status = 0 order by codigo";
        
        if(this.conectar()){
            this.sqlCosulta = this.conexion.prepareStatement(consulta);
            registros = this.sqlCosulta.executeQuery();
            
            while(registros.next()){
                Productos pro = new Productos();
                
                pro.setCodigo(registros.getString("codigo"));
                pro.setNombre(registros.getString("nombre"));
                pro.setPrecio(registros.getInt("precio"));
                pro.setFecha(registros.getString("fecha"));
                pro.setIdProductos(registros.getInt("idProductos"));
                pro.setStatus(registros.getInt("status"));
                
                // Agregarlo al ArrayList
                listaProductos.add(pro);
            }
        }
        // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        this.desconectar();
        return listaProductos;
    }

    @Override
    public Object buscar(String codigo) throws Exception {
        Productos pro = new Productos();
        String consulta = "Select * from productos where codigo = ? and status = 0";
        
        if(this.conectar()){
            this.sqlCosulta = this.conexion.prepareStatement(consulta);
            this.sqlCosulta.setString(1, codigo);
            
            registros = this.sqlCosulta.executeQuery();
            
            if(registros.next()){
                pro.setCodigo(codigo);
                pro.setNombre(registros.getString("nombre"));
                pro.setPrecio(registros.getInt("precio"));
                pro.setFecha(registros.getString("fecha"));
                pro.setIdProductos(registros.getInt("idProductos"));
                pro.setStatus(registros.getInt("status"));
            }
            
            this.desconectar();
        }
        
        return pro;
    }
}
